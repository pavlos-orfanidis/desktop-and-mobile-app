using System;
using System.Linq;
using System.Threading;
using System.IO;
using System.IO.Ports;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class SerialPortController : MonoBehaviour
{

    /**
     * Encoded characters to send less data over the serial:
     * 0: Number of grills
     * 
     */

    public TMP_Dropdown options;
    public TMP_Text output;
    public Button confirm;

    public TMP_InputField input;
    public Button send;

    public Text grillCount;

    public Button reset;

    private int numberOfGrills;


    private List<string> availableSerialPorts;
    private SerialPort selectedPort;

    private bool hasBeenCloned;

    // Start is called before the first frame update
    void Start()
    {
        hasBeenCloned = false;
        /*Reading all the available serial ports to a list*/
        availableSerialPorts = SerialPort.GetPortNames().ToList();


        send.onClick.AddListener(()=>{
            if (selectedPort == null)
            {
                return;
            }

            Debug.Log(input.text);
            selectedPort.WriteLine(input.text);
        });

        selectedPort = null;
        confirm.onClick.AddListener(setPort);
        RefreshPorts();
    }

    // Update is called once per frame
    void Update()
    {
        /**
         * This is for debugging purposes only. It should be removed and the populated reset should be called every time a new bit with zero arrives.
         */
        if (!hasBeenCloned)
        {
            populateReset();
        }



        /*Updating the list of available serial ports in case something has changed*/
        availableSerialPorts = SerialPort.GetPortNames().ToList();

        
        /**
         * If no pport has been selected return false
         */
        if (selectedPort==null)
        {
            RefreshPorts();
            grillCount.text = "-1";
            //confirm.interactable = true;
        }
        else
        {
            if (!availableSerialPorts.Contains(selectedPort.PortName))
            {
                selectedPort.Close();
                selectedPort = null;
                output.text = "� ������� ���� �����������. �� ��� ���� ��������� �� ������, ���� ����� ����. ������ ��� ������� ��������.";
                confirm.interactable = true;
            }
            else
            {

            }
        }

        
        // output.text += selectedPort.ReadLine();
    }



    void OnApplicationQuit()
    {
        Debug.Log("Application ending after " + Time.time + " seconds");
        if (selectedPort != null)
        {
            selectedPort.Close();
        }
    }



    void setPort()
    {
        confirm.interactable = false;
        selectedPort = new SerialPort("\\\\.\\" + options.options[options.value].text, 9600, Parity.None, 8, StopBits.One);
        try
        {
            selectedPort.Open();
            selectedPort.Handshake = Handshake.None;
            selectedPort.DataReceived += new SerialDataReceivedEventHandler(sp_DataReceived);
            Debug.Log("Serial Port opened successfully");

            /**
            * If you cannot open the port select another.
            * This can be done by setting the hasBeenPressed var to false, so that the first if is true.
            */
        }
        catch (IOException)
        {
            output.text = "I could not open the selected port. You should try again.";
            selectedPort = null;
        }
    }


    void populateReset()
    {
        numberOfGrills = 5;
        if (numberOfGrills < 1)
        {
            reset.gameObject.SetActive(false);
        }
        else
        {
            reset.GetComponentsInChildren<TMP_Text>()[0].text="�������� 1";
            Debug.Log("Hello");
            Button newReset=GameObject.Instantiate(reset);

            //reset.transform.parent.gameObject.AppendChild(newReset);
        }
        hasBeenCloned = true;
    }



    void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
        Thread.Sleep(500);
        char[] data = selectedPort.ReadLine().ToCharArray();
        //data = new char[] { '0','3' };
        if (data[0] == 0)
        {
            if (data[1] == '-')
            {
                grillCount.text = "0";
            }
            else
            {
                numberOfGrills = Int32.Parse(String.Join("", data.Skip(1).ToArray()))+1;
                grillCount.text = numberOfGrills.ToString();
                
            }
            
            print(data[1]+" number of grills");
        }
        // Invokes the delegate on the UI thread, and sends the data that was received to the invoked method.  
        // ---- The "si_DataReceived" method will be executed on the UI thread which allows populating of the textbox.  
        // output.text += data.Trim() + "\n";
    }

    void print(string text)
    {
        output.text += text;
    }

    /**
     * Function to refresh the ports in case one has not been selected. Maybe I shouldnt call the last line every time, but I will see if I can make that.
     */
    void RefreshPorts()
    {
        options.ClearOptions();
        options.AddOptions(availableSerialPorts);
    }
}